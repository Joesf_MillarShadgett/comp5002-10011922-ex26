﻿using System;
using System.Collections.Generic;


namespace Exercises
{



    public class Student
    {
        // I think I like vars up top
        string      name = "";
        int         idNum = -1;
        // Interface to vars just below that
        public string studentName
        {
            get {return name;}
            set {name = value;}
        }
        public int studentID
        {
            get {return idNum;}
            set {idNum = value;}
        }

        // Contructor as top method/function
        public Student(string _name, int _idNum)
        {
            name = _name;
            idNum = _idNum;
        }
        // Other peasant methods below that
        public void PrintInfo()
        {
            Console.WriteLine($"Student id number = {studentID}, student name = {studentName}.");
        }
        // c# is weird
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            Student s1 = new Student("Joesf", 1337);

            s1.PrintInfo();

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
